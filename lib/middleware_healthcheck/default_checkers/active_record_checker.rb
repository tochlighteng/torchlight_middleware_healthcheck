module MiddlewareHealthcheck
  module DefaultCheckers
    class ActiveRecordChecker
      attr_accessor :error

      def initialize(_app, _env)
      end

      def healthy?
        ActiveRecord::Base.connection.execute('select 1')
      rescue => e
        self.error = e.message
        false
      end
    end
  end
end
