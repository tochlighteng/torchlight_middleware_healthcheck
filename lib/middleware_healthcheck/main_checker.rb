module MiddlewareHealthcheck
  class MainChecker
    QUERY_STRING_KEY = "QUERY_STRING".freeze
    UNDEFINED_CHECKER_ERROR = "Can't find checker: ".freeze

    attr_accessor :app, :env

    def initialize(app, env)
      self.app = app
      self.env = env
    end

    def check_health
      if full_check?
        run_all_checkers
      elsif selected_check?
        run_selected_checkers
      end

      build_response
    end

    def build_response
      if errors.present?
        build_error_response
      else
        build_success_response
      end
    end

    def build_success_response
      build_text_response(
        configuration.success_response_status,
        configuration.success_response_body 
      )
    end

    def build_error_response
      build_text_response(
        configuration.error_response_status,
        errors.join(configuration.errors_delimiter)
      )
    end

    def build_text_response(status, body)
      [ status, { "Content-Type" => "text/plain" }, [body] ]
    end

    def run_all_checkers
      run_checkers(configuration.checkers)
    end

    def run_selected_checkers
      run_checkers(selected_checkers)
    end


    def run_checkers(checkers)
      checkers.each do |checker|
        checker_instance = checker.new(@app, @env)
        errors.push(checker_instance.error) unless checker_instance.healthy?
      end
    end

    def selected_checkers
      selected_checkers_names.map do |checker_name|
        find_checker_by_name(checker_name)
      end.compact
    end

    def find_checker_by_name(name)
      configuration.checkers.each do |checker|
        return checker if checker.to_s.demodulize.underscore.gsub(/_checker$/, '') == name
      end
      errors.push(UNDEFINED_CHECKER_ERROR + name.camelize)
      nil
    end

    def selected_checkers_names
      @selected_check_param_names ||= params[configuration.selected_check_param_name]
        .split(configuration.selected_check_param_split_delimiter)
    end

    def params
      @params ||= Rack::Utils.parse_nested_query(@env[QUERY_STRING_KEY])
    end

    def errors
      @errors ||= []
    end

    def full_check?
      params[configuration.full_check_param_name].present?
    end
    def selected_check?
      params[configuration.selected_check_param_name].present?
    end

    def configuration
      @configuration ||= MiddlewareHealthcheck.configuration
    end
  end
end
