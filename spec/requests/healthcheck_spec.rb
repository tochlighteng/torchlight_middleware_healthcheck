require 'rails_helper'

RSpec.describe 'Healthcheck middleware', type: :request do
  let(:mysql_error){ "MySQL server has gone away" }

  it "renders  middleware's response" do
    get '/healthcheck'
    expect(response.body).to include("It's alive!")
    expect(response.status).to eq 200

    allow(ActiveRecord::Base).to receive(:connection).and_raise(StandardError.new(mysql_error))

    get '/healthcheck?full=1'
    expect(response.body).to include(mysql_error)
    expect(response.status).to eq 422

    get '/healthcheck?checks=active_record'
    expect(response.body).to include(mysql_error)
    expect(response.status).to eq 422

    get '/healthcheck'
    expect(response.body).to include("It's alive!")
    expect(response.status).to eq 200

    allow(ActiveRecord::Base).to receive(:connection).and_call_original

    get '/healthcheck?full=1'
    expect(response.body).to include("It's alive!")
    expect(response.status).to eq 200

    get '/healthcheck?checks=active_record'
    expect(response.body).to include("It's alive!")
    expect(response.status).to eq 200
  end

  it 'renders error if checker is undefined' do
    get '/healthcheck?checks=active_record,something_funny,redis'
    expect(response.body).to include("Can't find checker: SomethingFunny")
    expect(response.body).to include("Can't find checker: Redis")
  end

  it 'renders standard response' do
    get '/'
    expect(response.body).to include('Home')
  end
end
